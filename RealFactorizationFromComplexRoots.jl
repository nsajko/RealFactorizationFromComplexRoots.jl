# Copyright 2022 Neven Sajko. All rights reserved.

module RealFactorizationFromComplexRoots

import Combinatorics

export
  PolDeg1,
  PolDeg2,
  to_tuple,
  to_tuples,
  RealFactorization,
  fact_of_type,
  real_factorization_from_complex_roots

# Monic polynomial of degree one.
struct PolDeg1{F <: AbstractFloat}
  con::F
end

to_tuple(p::PolDeg1{F}) where {F <: AbstractFloat} =
  (p.con, one(F))

pol_of_type(::Type{New},
            p::PolDeg1{Old}) where {New <: AbstractFloat,
                                    Old <: AbstractFloat} =
  PolDeg1(New(p.con))

# Monic polynomial of degree two.
struct PolDeg2{F <: AbstractFloat}
  con::F
  lin::F
end

to_tuple(p::PolDeg2{F}) where {F <: AbstractFloat} =
  (p.con, p.lin, one(F))

pol_of_type(::Type{New},
            p::PolDeg2{Old}) where {New <: AbstractFloat,
                                    Old <: AbstractFloat} =
  PolDeg2(New.((p.con, p.lin))...)

struct RealFactorization{F <: AbstractFloat}
  factors_deg1::Vector{PolDeg1{F}}
  factors_deg2::Vector{PolDeg2{F}}
end

to_tuples(f::RealFactorization{F}) where {F <: AbstractFloat} =
  map.(to_tuple, (f.factors_deg1, f.factors_deg2))

fact_of_type(::Type{New},
             f::RealFactorization{Old}) where {New <: AbstractFloat,
                                               Old <: AbstractFloat} =
  RealFactorization(map.(x -> pol_of_type(New, x),
                         (f.factors_deg1, f.factors_deg2))...)

# Opposite of the imaginary part of the coefficient in the linear term
# of the product of the two degree-one polynomials.
product_lin_coef(p::NTuple{2, Complex{F}}) where {F <: AbstractFloat} =
  imag(p[1]) + imag(p[2])

# The imaginary part of the coefficient in the constant term of the
# product of the two degree-one polynomials.
product_con_coef(p::NTuple{2, Complex{F}}) where {F <: AbstractFloat} =
  real(p[1]) * imag(p[2]) +
  real(p[2]) * imag(p[1])

squared_error_deg2(v::V,
                   ind::NTuple{2, W}) where {F <: AbstractFloat,
                                             V <: AbstractVector{Complex{F}},
                                             W <: AbstractVector{<: Signed}} =
  sum(p -> product_lin_coef(p)^2 + product_con_coef(p)^2,
      zip((view(v, ind[1]), view(v, ind[2]))...),
      init = zero(F))

squared_error_deg1(v::V,
                   ind::W) where {F <: AbstractFloat,
                                  V <: AbstractVector{Complex{F}},
                                  W <: AbstractVector{<: Signed}} =
  sum(c -> imag(c)^2,
      view(v, ind),
      init = zero(F))

squared_error(roots::V,
              ind::Tuple{W, NTuple{2, U}}) where {F <: AbstractFloat,
                                                  V <: AbstractVector{Complex{F}},
                                                  W <: AbstractVector{<: Signed},
                                                  U <: AbstractVector{<: Signed}} =
  squared_error_deg1(roots, ind[1]) +
  squared_error_deg2(roots, ind[2])

struct ComparisonHelper{C <: Any, P <: Any}
  for_comparing::C
  payload::P

  ComparisonHelper(for_comparing::C, payload::P) where {C <: Any, P <: Any} =
    new{C, P}(for_comparing, payload)
end

Base.isless(l::CH, r::CH) where {C <: Any,
                                 CH <: ComparisonHelper{C}} =
  Base.isless(l.for_comparing, r.for_comparing)

unpack(ch::CH) where {CH <: ComparisonHelper} =
  ch.payload

partitioned_indices(ind::NTuple{2, W},
                    perm::W) where {W <: AbstractVector{<: Signed}} =
  (setdiff(ind[2], perm),
   (ind[1], perm))

squared_error_with_indices(roots::V,
                           ind::Tuple{W, NTuple{2, U}}) where
{F <: AbstractFloat,
 V <: AbstractVector{Complex{F}},
 W <: AbstractVector{<: Signed},
 U <: AbstractVector{<: Signed}} =
  ComparisonHelper(squared_error(roots, ind), ind)

min_error(roots::V,
          ind::NTuple{2, W}) where {F <: AbstractFloat,
                                    V <: AbstractVector{Complex{F}},
                                    W <: AbstractVector{<: Signed}} =
  unpack(minimum(
    let roots = roots, ind = ind
      perm -> squared_error_with_indices(roots, partitioned_indices(ind, perm))
    end,
    Combinatorics.permutations(ind[2], length(ind[1])),
    init = ComparisonHelper(F(Inf), (Int8[], (Int8[], Int8[])))))

RealFactorization_impl1(roots::V,
                  ind::Tuple{W, NTuple{2, U}}) where
{F <: AbstractFloat,
 V <: AbstractVector{Complex{F}},
 W <: AbstractVector{<: Signed},
 U <: AbstractVector{<: Signed}} =
  RealFactorization(
    ifelse(
      isempty(ind[1]),
      PolDeg1{F}[],
      map(
        r -> PolDeg1(-real(r)),
        view(roots, ind[1]))),
    ifelse(
      isempty(ind[2][1]),
      PolDeg2{F}[],
      map(
        p -> PolDeg2(real(p[1])*real(p[2]) - imag(p[1])*imag(p[2]),
                     -(real(p[1]) + real(p[2]))),
        zip(view(roots, ind[2][1]),
            view(roots, ind[2][2])))))

minmax_by_length(pair::NTuple{2, V}) where {V <: AbstractVector} =
  ifelse(length(pair[2]) < length(pair[1]),
         reverse(pair),
         pair)

merged_best_indices(i1::Tuple{V, <: NTuple{2}},
                    i2::V) where {V <: AbstractVector{<: Signed}} =
  (vcat(i2, i1[1]), i1[2])

RealFactorization_impl(roots::V,
                  part_ind::NTuple{3, W}) where
{F <: AbstractFloat,
 V <: AbstractVector{Complex{F}},
 W <: AbstractVector{<: Signed}} =
  RealFactorization_impl1(
    roots,
    merged_best_indices(
      min_error(roots,
                minmax_by_length(part_ind[1:2])),
      part_ind[3]))

# Partitions complex numbers by the sign of the imaginary part.
function partition_by_sign(roots::V) where {F <: AbstractFloat,
                                            V <: AbstractVector{Complex{F}}}
  local lt = Int8[]
  local gt = Int8[]
  local eq = Int8[]
  for i in eachindex(roots)
    local s = sign(imag(roots[i]))
    if s < 0
      push!(lt, i)
    elseif 0 < s
      push!(gt, i)
    else
      push!(eq, i)
    end
  end
  (lt, gt, eq)
end

# Real factorization from complex roots.
RealFactorization(roots::V) where {F <: AbstractFloat,
                                   V <: AbstractVector{Complex{F}}} =
  RealFactorization_impl(roots, partition_by_sign(roots))

end  # module RealFactorizationFromComplexRoots
